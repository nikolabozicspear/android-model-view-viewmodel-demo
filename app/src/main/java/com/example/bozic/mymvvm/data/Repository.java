package com.example.bozic.mymvvm.data;

import android.arch.lifecycle.LiveData;
import java.util.List;

public class Repository {

    private final ListItemDao listItemDao;

    public Repository(ListItemDao listItemDao) {
        this.listItemDao = listItemDao;
    }

    public LiveData<List<ListItem>> getListOfData() {
        return listItemDao.getListItems();
    }

    public Long createNewListItem(ListItem listItem) {
        return listItemDao.insertItem(listItem);
    }

    public LiveData<ListItem> getListItem(String itemId) {
        return listItemDao.getItemById(itemId);
    }
}
package com.example.bozic.mymvvm.create;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import com.example.bozic.mymvvm.MvvmApplication;
import com.example.bozic.mymvvm.R;
import com.example.bozic.mymvvm.data.ListItem;
import com.example.bozic.mymvvm.data.ListItemViewModel;
import com.example.bozic.mymvvm.list.ListActivity;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateFragment extends Fragment {

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    ListItemViewModel listItemViewModel;

    @BindView(R.id.editText)
    EditText et;

    public CreateFragment() {
    }

    public static CreateFragment newInstance() {
        return new CreateFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((MvvmApplication) getActivity().getApplication())
                .getApplicationComponent()
                .inject(this);

    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Set up and subscribe (observe) to the ViewModel
        listItemViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(ListItemViewModel.class);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_create, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @OnClick(R.id.button2)
    public void onEvaluateClick(Button v) {
        ListItem listItem = new ListItem(
                getDate(), et.getText().toString()

        );

        listItemViewModel.addNewItemToDatabase(listItem);

        startDetailActivity();
    }

    private void startDetailActivity() {
        Activity container = getActivity();
        Intent i = new Intent(container, ListActivity.class);
        startActivity(i);
    }

    public String getDate() {
        Date currentDate = Calendar.getInstance().getTime();
        DateFormat format = new SimpleDateFormat("yyyy/MM/dd/kk:mm:ss");
        return format.format(currentDate);
    }
}

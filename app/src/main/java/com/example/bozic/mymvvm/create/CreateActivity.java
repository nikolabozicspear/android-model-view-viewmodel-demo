package com.example.bozic.mymvvm.create;

import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import com.example.bozic.mymvvm.BaseActivity;
import com.example.bozic.mymvvm.R;

public class CreateActivity extends BaseActivity {
    private static final String TAG = "TAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        FragmentManager manager = getSupportFragmentManager();
        CreateFragment fragment = (CreateFragment) manager.findFragmentByTag(TAG);

        if (fragment == null) {
            fragment = CreateFragment.newInstance();
        }

        addFragmentToActivity(manager,
                fragment,
                R.id.root_activity_create,
                TAG
        );
    }
}

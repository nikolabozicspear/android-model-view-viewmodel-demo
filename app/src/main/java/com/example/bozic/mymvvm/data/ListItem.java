package com.example.bozic.mymvvm.data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class ListItem {

    @PrimaryKey
    @NonNull
    private String itemId;
    private String name;

    public ListItem(@NonNull String itemId, String name) {
        this.itemId = itemId;
        this.name = name;
    }

    @NonNull
    public String getItemId() {
        return itemId;
    }

    public void setItemId(@NonNull String itemId) {
        this.itemId = itemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ListItem{" +
                "itemId='" + itemId + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}

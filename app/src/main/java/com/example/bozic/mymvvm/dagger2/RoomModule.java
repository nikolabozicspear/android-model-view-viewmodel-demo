package com.example.bozic.mymvvm.dagger2;
import android.app.Application;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.persistence.room.Room;
import com.example.bozic.mymvvm.data.CustomViewModelFactory;
import com.example.bozic.mymvvm.data.ListItemDao;
import com.example.bozic.mymvvm.data.ListItemDataBase;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;


@Module
public class RoomModule {

    private final ListItemDataBase database;

    public RoomModule(Application application) {
        this.database = Room.databaseBuilder(
                application,
                ListItemDataBase.class,
                "ListItem.db"
        ).fallbackToDestructiveMigration().build();
    }

    @Provides
    @Singleton
    ListItemDataBase provideDataBase(Application application){
        return database;
    }

    @Provides
    @Singleton
    ListItemDao provideUserDao(ListItemDataBase database){
        return database.listItemDao();
    }

    @Provides
    @Singleton
    com.example.bozic.mymvvm.data.Repository provideListItemRepository(ListItemDao listItemDao){
        return new com.example.bozic.mymvvm.data.Repository(listItemDao);
    }

    @Provides
    @Singleton
    ViewModelProvider.Factory provideViewModelFactory(com.example.bozic.mymvvm.data.Repository repository){
        return new CustomViewModelFactory(repository);
    }
}

package com.example.bozic.mymvvm.data;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {ListItem.class}, version = 2)
public abstract class ListItemDataBase extends RoomDatabase{
    public abstract ListItemDao listItemDao();
}

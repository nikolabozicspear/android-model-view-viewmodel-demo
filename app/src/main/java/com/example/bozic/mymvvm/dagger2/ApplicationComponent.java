package com.example.bozic.mymvvm.dagger2;

import android.app.Application;
import com.example.bozic.mymvvm.create.CreateFragment;
import com.example.bozic.mymvvm.list.ListFragment;
import javax.inject.Singleton;
import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, RoomModule.class})
public interface ApplicationComponent {

    void inject(CreateFragment fragment);
    void inject(ListFragment createFragment);

    Application application();
}

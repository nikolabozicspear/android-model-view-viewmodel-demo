package com.example.bozic.mymvvm.list;

import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import com.example.bozic.mymvvm.BaseActivity;
import com.example.bozic.mymvvm.R;

public class ListActivity extends BaseActivity {

    private static final String DETAIL_FRAG = "DETAIL_FRAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail);

        FragmentManager manager = getSupportFragmentManager();

        ListFragment fragment = (ListFragment) manager.findFragmentByTag(DETAIL_FRAG);

        if (fragment == null) {
            fragment = ListFragment.newInstance();
        }

        addFragmentToActivity(manager,
                fragment,
                R.id.root_activity_detail,
                DETAIL_FRAG
        );
    }
}

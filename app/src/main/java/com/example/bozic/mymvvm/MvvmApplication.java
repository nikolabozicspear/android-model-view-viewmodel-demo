package com.example.bozic.mymvvm;

import android.app.Application;
import com.example.bozic.mymvvm.dagger2.ApplicationComponent;
import com.example.bozic.mymvvm.dagger2.ApplicationModule;
import com.example.bozic.mymvvm.dagger2.DaggerApplicationComponent;
import com.example.bozic.mymvvm.dagger2.RoomModule;
public class MvvmApplication extends Application {
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .roomModule(new RoomModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

}

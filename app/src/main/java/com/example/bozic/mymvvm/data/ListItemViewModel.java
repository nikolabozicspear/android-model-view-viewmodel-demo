package com.example.bozic.mymvvm.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.os.AsyncTask;
import java.util.List;

public class ListItemViewModel extends ViewModel {
    private Repository repository;

    public ListItemViewModel(Repository repository) {
        this.repository = repository;
    }

    public void addNewItemToDatabase(ListItem listItem){
        new AddItemTask().execute(listItem);
    }

    public LiveData<List<ListItem>> getListItems() {
        return repository.getListOfData();
    }

    public LiveData<ListItem> getListItemById(String itemId){
        return repository.getListItem(itemId);
    }

    private class AddItemTask extends AsyncTask<ListItem, Void, Void> {
        @Override
        protected Void doInBackground(ListItem... item) {
           repository.createNewListItem(item[0]);
            return null;
        }
    }
}

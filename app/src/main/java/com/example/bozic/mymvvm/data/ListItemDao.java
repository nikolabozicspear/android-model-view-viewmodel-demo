package com.example.bozic.mymvvm.data;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import java.util.List;
import static android.arch.persistence.room.OnConflictStrategy.REPLACE;


@Dao
public interface ListItemDao {

    @Insert(onConflict = REPLACE)
    Long insertItem(ListItem ListItem);

    @Query("SELECT * FROM ListItem WHERE itemId = :itemId")
    LiveData<ListItem> getItemById(String itemId);

    @Query("SELECT * FROM ListItem")
    LiveData<List<ListItem>> getListItems();

    @Query("SELECT * FROM ListItem")
    LiveData<List<ListItem>> getAllItems();

    @Delete
    void deleteUser(ListItem listItem);
}

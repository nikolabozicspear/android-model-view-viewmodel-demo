package com.example.bozic.mymvvm.dagger2;
import android.app.Application;
import com.example.bozic.mymvvm.MvvmApplication;
import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {
    private final MvvmApplication application;
    public ApplicationModule(MvvmApplication application) {
        this.application = application;
    }

    @Provides
    MvvmApplication provideRoomDemoApplication(){
        return application;
    }

    @Provides
    Application provideApplication(){
        return application;
    }
}
